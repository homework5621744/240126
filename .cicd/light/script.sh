#!/bin/bash

###   Update and config OS   ###
      sudo apt update
      sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
      sudo systemctl restart sshd
###   Install nvm and node 16 aka gallium   ###
      cd /home/vagrant
      curl -sL https://deb.nodesource.com/setup_16.x -o /tmp/nodesource_setup.sh
      sudo bash /tmp/nodesource_setup.sh
      sudo apt install nodejs
      node -v
###   Clone and install game 2040   ###
      git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git
      cd /home/vagrant/2048-game
      npm install --include=dev
      npm run build
      echo "
[Unit]
Description=2048-game

[Service]
WorkingDirectory=/home/vagrant/2048-game
ExecStart=npm start
Restart=Always

[Install]
WantedBy=multi-user.target" | sudo tee /etc/systemd/system/2048-game.service
      sudo systemctl daemon-reload
      sudo systemctl enable 2048-game.service
      sudo systemctl start 2048-game.service
###   Install nginx  and configure proxy to localhost   ###
      sudo apt install -y nginx
      sudo ufw allow 'Nginx HTTP'
      echo "
#user  nobody;
worker_processes  1;

events {
    worker_connections  1024;
}
http {
    include       mime.types;
    default_type  application/octet-stream;
    send_timeout 1800;
    sendfile        on;
    keepalive_timeout  6500;

    server {
        listen       3000;
        server_name  localhost;

        location / {
          proxy_pass          http://localhost:8080;
          proxy_set_header    X-Forwarded-Proto http;
          proxy_read_timeout 1800;
          proxy_connect_timeout 1800;
        }
    }
}" | sudo tee /etc/nginx/nginx.conf
      sudo nginx -t
      sudo systemctl restart nginx
